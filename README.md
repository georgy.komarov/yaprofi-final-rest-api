# yaprofi_final

## Локальный запуск
Установка зависимостей:
```shell
pip install -r requirements.txt
```

Запуск Django приложения на порту 8080:
```shell
python manage.py runserver 8080
```

## Запуск через docker-compose с СУБД SQLite
```shell
docker-compose up -d --build
```

## Swagger
Swagger доступен по пути http://127.0.0.1:8080/swagger/
