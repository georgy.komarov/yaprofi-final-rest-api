from drf_spectacular.utils import extend_schema

from main.serializers import DetailSerializer, ParticipantSerializer

groups_list_schema = extend_schema(summary='Получить список групп', tags=['group'])
groups_create_schema = extend_schema(summary='Создать группу')
groups_retrieve_schema = extend_schema(summary='Получить группу по ID')
groups_update_schema = extend_schema(summary='Редактировать группу')
groups_partial_update_schema = extend_schema(exclude=True)
groups_destroy_schema = extend_schema(summary='Удалить группу')
groups_toss_schema = extend_schema(summary='Провести жеребьевку в группе', request=None,
                                   responses={200: ParticipantSerializer(many=True), 409: DetailSerializer})

groups_participant_create_schema = extend_schema(summary='Добавить участника в группу', tags=['group-participant'])
groups_participant_destroy_schema = extend_schema(summary='Удалить участника из группы', tags=['group-participant'])
groups_participant_recipient_schema = extend_schema(summary='Получить подопечного по ID участника', tags=['group-participant'])
