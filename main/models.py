from django.db import models


class Group(models.Model):
    name = models.TextField(verbose_name='Название группы')
    description = models.TextField(null=True, blank=True, verbose_name='Описание группы')


class Participant(models.Model):
    name = models.TextField(verbose_name='Имя участника')
    wish = models.TextField(null=True, blank=True, verbose_name='Пожелание')
    recipient = models.ForeignKey('Participant', on_delete=models.CASCADE, null=True, blank=True, verbose_name='Подопечный')
    group = models.ForeignKey('Group', related_name='participants', on_delete=models.CASCADE, verbose_name='Группа')
