from drf_spectacular.utils import extend_schema_view
from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from main.models import Group, Participant
from main.openapi import *
from main.serializers import GroupSerializer, GroupWithParticipantsSerializer, ParticipantSerializer


@extend_schema_view(
    create=groups_create_schema,
    retrieve=groups_retrieve_schema,
    update=groups_update_schema,
    partial_update=groups_partial_update_schema,
    destroy=groups_destroy_schema,
    toss=groups_toss_schema,
)
class GroupViewSet(mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.DestroyModelMixin,
                   GenericViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return GroupWithParticipantsSerializer
        elif self.action == 'toss':
            return ParticipantSerializer
        return self.serializer_class

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        return Response(instance.pk, status=status.HTTP_201_CREATED)

    @action(detail=True, methods=['POST'])
    def toss(self, request, *args, **kwargs):
        instance = self.get_object()
        participants = list(instance.participants.all())
        participants_count = len(participants)

        if participants_count < 3:
            return Response(data={'detail': 'Проведение жеребьёвки в данный момент невозможно.'}, status=status.HTTP_409_CONFLICT)

        for i, participant in enumerate(participants, start=1):
            if i == participants_count:
                participant.recipient = participants[0]
            else:
                participant.recipient = participants[i]
            participant.save()

        serializer = self.get_serializer(instance.participants.all(), many=True)
        return Response(serializer.data)


@extend_schema_view(
    list=groups_list_schema,
)
class GroupCreateViewSet(mixins.ListModelMixin, GenericViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


@extend_schema_view(
    create=groups_participant_create_schema,
    destroy=groups_participant_destroy_schema,
    recipient=groups_participant_recipient_schema,
)
class ParticipantViewSet(mixins.CreateModelMixin, mixins.DestroyModelMixin, GenericViewSet):
    queryset = Participant.objects.all()
    serializer_class = ParticipantSerializer

    def get_group(self):
        return get_object_or_404(Group, pk=self.kwargs['group_pk'])

    def get_queryset(self):
        return self.get_group().participants.all()

    def create(self, request, *args, **kwargs):
        group_instance = self.get_group()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save(group_id=group_instance.pk)
        return Response(instance.pk, status=status.HTTP_201_CREATED)

    @action(detail=True, methods=['GET'])
    def recipient(self, request, *args, **kwargs):
        participant_instance = self.get_object()
        recipient_instance = participant_instance.recipient
        if recipient_instance is None:
            raise NotFound('В данной группе ещё не проведена жеребьёвка или подопечный был удалён.')
        else:
            serializer = self.get_serializer(recipient_instance)
            return Response(serializer.data)
