from django.contrib import admin

from main.models import Group, Participant


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    pass


@admin.register(Participant)
class ParticipantAdmin(admin.ModelAdmin):
    pass
