from rest_framework import serializers

from main.models import Group, Participant


class DetailSerializer(serializers.Serializer):
    detail = serializers.CharField(label='Сообщение')


class RecipientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = ['id', 'name', 'wish']


class ParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = ['id', 'name', 'wish', 'recipient']

    recipient = RecipientSerializer(read_only=True)


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name', 'description']


class GroupWithParticipantsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name', 'description', 'participants']

    participants = ParticipantSerializer(read_only=True, many=True)
